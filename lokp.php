<?php
include 'project_1.php';
$siswa=$db->query("SELECT * from traveling");   
$data_siswa=$siswa->fetchAll();

// if (isset())
// var_dump($data_siswa);
// exit;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- css for font awsome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <style>
        body{
    background-image: url(image/bu.jpg);
    background-size: cover;
}
.pro{
    text-indent: 20px;
}
#header{
    background-color: #ffa800;
    color: #fff;
    width:100%;
    position: fixed;
    /* top: 17 ; */
    /* left: 0; */
    /* margin: 70; */
    /* text-align:left; */
    /* letter-spacing: 0px; */
    z-index: 70;
}
#header.sticky{
    padding: 5px 100px;
    background: #000000	;
}

#pue{
        padding:70px 0px 0px 50px;
}
#judul{
    z-index: -1;
}
.digital{
    padding:70px 0px 100px 60px;
}
#pembuka{
    text-align: center;
    padding:20px 20px 90px 0px;
    margin-left: 34px;
    margin-right: 34px;
    margin-top: -80px;
    background: rgba(0, 0, 0, 0.5);
}
#home{
    background:#f00;
}
#wisata1{
    margin-bottom: 20px;
    margin-right: 60px;
}
/* untuk lokp */
#headerlokpp{
    background-image: url(image/bglokpp.jpg);
    padding: 10px 10px 10px 10px    ;
}
.kepler{
    background-image: url(image/bglok.png);
    background-size: cover;
    
}
nav.navbar{
    /* background-color: slategrey; */
    background-image: url(image/po.jpg);
}
#textl{
    margin-top: 70px;
    margin-left: 18px;
}
#box1{
    width:100px;
    height:220px;
    background:rgba(0, 0, 0, 0.5);
    margin-top: 100px;
}

    </style>
    <!-- css yg font -->
    <link rel="stylesheet" href="font.css">
    <title>Document</title>
</head>
<body>
<nav  id="headerlokpp"class="navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link bg-danger" href="index.php">Beranda</a>
      </li>
    </ul>
  </div>
</nav>
<!-- Just an image -->
<div class="container">
    <div class="row">
        <div class="col-5">

            <form class="kepler" action="pembayaran.php" method="POST">
                    <nav class="navbar ">
                        <p>Data pembayaran</p>
                    </nav>
                <div class="form-group">
                    <label for="exampleInputEmail1" >Nama kota</label>
                    <input type="text" name="nama_kota" class="form-control"required autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Daerah</label>
                    <input type="text" name="nama_daerah" class="form-control"required autocomplete="off">
                </div>
            <!-- Button trigger modal -->

            <div class="form-group">
                <label for="exampleInputEmail1">Menginap</label>
                <input type="text" name="penginapan" class="form-control"required autocomplete="off">
            </div>
        <button type="submit" name="action" class="btn btn-primary">Simpan</button>
            </form>
        </div>
        <div class="col-6">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
      <div class="carousel-item active">
          <div class="card bg-dark text-white">
              <img src="image/b2.jpg" class="card-img" alt="..."height="380px">
              <div class="card-img-overlay"id="box1"></div>
              <div id="textl" class="card-img-overlay">
                  <h5 class="card-title">Bali</h5>
                  <p class="card-text">Pulau Bali adalah salah satu pulau dari 17.000 lebih kepulauan yang ada di Indonesia. Dengan luas pulau sepanjang 153 km dan selebar 112 km dan luas pulau 123,98 km2.</p>
                  <p class="card-text">Wisata Bali</p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="card bg-dark text-white">
                <img src="image/dana1.jpg" class="card-img" alt="..."height="380px">
                <div class="card-img-overlay"id="box1"></div>
                <div id="textl" class="card-img-overlay">
                    <h5 class="card-title">Danau Toba</h5>
                    <p class="card-text">Indonesia memiliki beragam keindahan alam luar biasa yang tidak dimiliki negara-negara lainnya. Salah satunya, yang bisa Anda jadikan destinasi liburan tahun ini adalah Danau Toba.</p>
                    <p class="card-text">Wisata Danau Toba</p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="card bg-dark text-white">
                <img src="image/RJ.jpg" class="card-img" alt="...">
                <div class="card-img-overlay"id="box1"></div>
                <div id="textl" class="card-img-overlay">
                    <h5 class="card-title">Raja Ampat</h5>
                    <p class="card-text">Raja Ampat merupakan rangkaian empat gugusan pulau yang berdekatan dan berlokasi di barat bagian Kepala Burung (Vogelkoop) Pulau Papua.</p>
                    <p class="card-text">Wisata Raja Ampat</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>