<?php
include 'project_1.php';

$curl = curl_init();
curl_setopt($curl,CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result,true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$TotalConfirmed =(string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$TotalDeaths =(string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$TotalRecovered =(string)$global['TotalRecovered'];
$countries =$result['Countries'];

$siswa=$db->query("select * from traveling"); // Prepare statement

$data_siswa=$siswa->fetchAll(); //execute and get data as array


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- css for font awsome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- link manggil css untuk font -->
    <link rel="stylesheet" href="css/font.css">
    <!-- css panggil  -->
    <style>
      body{
    background-image: url(/image/bu.jpg);
    background-size: cover;
}
.pro{
    text-indent: 20px;
}
#header{
    background-color: #ffa800;
    color: #fff;
    width:100%;
    position: fixed;
    /* top: 17 ; */
    /* left: 0; */
    /* margin: 70; */
    /* text-align:left; */
    /* letter-spacing: 0px; */
    z-index: 70;
}
#header.sticky{
    padding: 5px 100px;
    background: #000000	;
}

#pue{
        padding:70px 0px 0px 50px;
}
#judul{
    z-index: -1;
}
.digital{
    padding:70px 0px 100px 60px;
}
#pembuka{
    text-align: center;
    padding:20px 20px 90px 0px;
    margin-left: 34px;
    margin-right: 34px;
    margin-top: -80px;
    background: rgba(0, 0, 0, 0.5);
}
#home{
    background:#f00;
}
#wisata1{
    margin-bottom: 20px;
    margin-right: 60px;
}
/* untuk lokp */
#headerlokpp{
    background-image: url(/image/bglokpp.jpg);
    padding: 10px 10px 10px 10px    ;
}
.kepler{
    background-image: url(/image/bglok.png);
    background-size: cover;
    
}
nav.navbar{
    /* background-color: slategrey; */
    background-image: url(/image/po.jpg);
}
#textl{
    margin-top: 70px;
    margin-left: 18px;
}
#box1{
    width:100px;
    height:220px;
    background:rgba(0, 0, 0, 0.5);
    margin-top: 100px;
}

    </style>
    <title>TRAVELYos</title>
</head>
<body> 
  <!-- navbar -->
 <nav id="header" class="navbar navbar-expand-lg navbar-light bg-dark">
  <a id="home"class="navbar-brand text-light" href=""><i class="fas fa-home"></i>ome</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-shopping-cart"></i>Berlanggan
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="lokp.php">Pesan</a>
            <a class="dropdown-item" href="pembayaran.php">Edit Pemesanan</a>
        </li>
      </ul>
  </div>
</nav>
  <!-- ========================================nav judul======================================== -->
  <nav id="judul"class="navbar navbar-light bg-dark text-light">
    <h2 class="digital">TRAVELYos</h2>
  </nav> 
  <div id="pembuka" class="conatiner-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="text-white">
                  <div class="" >
                    <h5 class="card-title">Slamat datang di TRAVELYos</h5>
                    <!-- <p class="card-text">Silahkan cari tempat wisata yg mau di kunjungi</p> -->
                    <p class="card-text">Mari kita lawan dan hadapi semua mulai dari diri kita sendiri.</p>
                    <p class="card-text">Tetap waspada dan tidak panik</p>
                    <p class="card-text">Hindari keramaian baik itu tempat tertutup maupun tempat terbuka</p>
                    <p class="card-text mt-20px">&copy;Travel_lagih</p>
                  </div>
            </div>
          </div>
        </div>
      </div>
      <!-- As a heading -->
<nav class="navbar-light bg-primary">
  <span class="navbar-brand mb-0 h1">Himbauan sebelum Traveling</span>
</nav>
      <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <i class="fas fa-file-word"> Peringatan</i>
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
      Traveling bisa menjadi hal yang menyenangkan, namun traveling juga bisa membuat Anda merasa stres terutama jika Anda melakukan beberapa kesalahan. 
 
 Dan seberapa matangnya persiapan Anda sebelum traveling, tentu ada kemungkinan traveling Anda tidak berjalan dengan lancar. 
  
 Untuk itu, sangat penting mengetahui kesalahan-kesalahan apa yang sering dilakukan orang saat sedang traveling agar Anda tidak melakukannya di perjalanan Anda selanjutnya.
       </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <i class="fas fa-file-word"> Bimbingan</i>
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <ul>
          <li>Atur Jadwal</li>
          <li>Tentukan Destinasi</li>
          <li>Menyusun Anggaran Biaya Liburan</li>
          <li>Membuat List Barang Bawaan</li>
          <li>Salin Kartu Identitas dan Dokumen Berharga</li>
          <li> Bawa Baju dan Sepatu Olahraga</li>
          <li> Jaga Stamina dan Kesehatan Tubuh</li>
          <li> Obat Pribadi</li>
        </ul>
    </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <i class="fas fa-file-word"> Penutup</i>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
      Saya yakin banyak dari Anda yang sudah tidak sabar untuk kembali traveling, saya juga termasuk. Tidak terasa sudah berbulan-bulan harus menghabiskan waktu di rumah.

Kembali, saya tidak menyarankan untuk bepergian, apalagi ke luar negeri saat ini. Rumitnya persyaratan dan resiko nyata terkena COVID-19 tentu tidak worth it.

Semakin cepat pandemi ini usai, maka semakin cepat juga kehidupan kita kembali normal. Saat ini, kita bisa berkontribusi dengan memutus rantai penyebaran dan berharap agar vaksin bisa segera dituntaskan.

Saya sarankan bagi Anda yang benar-benar akan ke luar negeri untuk juga mengecek informasi larangan dan persyaratan traveling melalui sumber lain, misalnya dari kedutaan perwakilan negara.
      </div>
    </div>
  </div>
</div>
  <div id="wisata1"class="container-fluid">
   <div class="row">
     <div class="col-md-3">
       <a href="https://www.gotravelaindonesia.com/informasi-umum-tentang-bali/">
        <div class="card" style="width: 17rem;">
            <img src="image\b1.jpg" title="Mau tau Tentang Bali?" class="card-img-top" alt="" height="250px">
        <div class="card-body">
           <h4>Bali</h4>
        </div>
        </div>
        </a>
     </div>
    <div class="col-md-3">
      <a href="https://www.indonesiakaya.com/jelajah-indonesia/detail/raja-ampat-surga-petualangan-dunia-di-ujung-papua">
     <div class="card" style="width: 17rem;">
          <img src="\image\RJ.jpg" class="card-img-top" alt="" height="250px">
        <div class="card-body">
          <h4>Raja Ampat</h4>
        </div>
     </div>
     </a>
   </div>
     <div class="col-md-3">
       <a href="https://travel.kompas.com/read/2020/09/12/185700527/menyibak-keindahan-danau-toba-surga-petualangan-di-tanah-tapanuli-">
     <div class="card" style="width: 17rem;">
          <img src="\image\dana1.jpg" class="card-img-top" alt="" height="250px">
        <div class="card-body">
          <h4>Danau Toba</h4>
        </div>
     </div>
       </a>
     </div>
     <div class="container-fluid"> 
                        <div class="row">
                            <h4 id="ab" >Data Covid Seluruh Dunia<p id="tgl">
                                <?php echo date('d F Y'); ?>
                            </p></h4>
                            <canvas id="myChart" style="height: 50%; width: 50%;"></canvas>
                        </div>
                    </div>
     </div>
   </div>
 <!-- </div> -->
      <section class="container-fluid">
        <div class="row mt-5 bg-dark">
          <div class="col-md-4">
            <h2 class="display-5 tesxt-center text-white">Location</h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31716.14370438818!2d110.74489389599503!3d-6.4558608557267325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e713cdfd1e74769%3A0x4921a1cef211276a!2sDuren%2C%20Tubanan%2C%20Kembang%2C%20Kabupaten%20Jepara%2C%20Jawa%20Tengah!5e0!3m2!1sid!2sid!4v1599297738956!5m2!1sid!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="col-md-4">
              <i class="display-5 tesxt-center text-white">kontak</i>
              <div>
                <i class="fab fa-whatsapp-square" style="font-size:40px;color:white"></i>
                <a href="6285291241657" target="_blanks" >+62 852 9124 1657</a>
              </div>
            </div>
              <div class="col-md-4">
                <i class="display-5 tesxt-center text-white">sosial media</i>
                <div>
                  <i class="fab fa-facebook" style="font-size:40px;color:white"></i>
                  <a href="https://m.facebook.com/yosep.adi.794" target="_blanks">adi yosafat</a>
              </div>
            </div>
        </div>   
      </section>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">    
 window.addEventListener("scroll",function(){
   var header = document.querySelector("nav");
   header.classList.toggle("sticky", window.scrollY > 0);
 })

        var ctx = document.getElementById('myChart').getContext('2d')

        var covid = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache : false
        })

        .done(function (canvas) {
            
            
            function getContries(canvas) {
                var show_country=[];

                canvas.Countries.forEach(function(el) {
                    show_country.push(el.Country);
                })
                return show_country;
            }


            function getHealth(canvas) {
                var recovered=[];

                canvas.Countries.forEach(function(el) {
                    recovered.push(el.TotalRecovered)
                })
                return recovered;
            }   
            
            var color=[];
            function color_random(){
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            } 

            for ( var i in canvas.Countries) {
                color.push(color_random());
            }


            var myChart = new Chart(ctx,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# of Votes',
                        data: getHealth(canvas),
                        backgroundColor: color,
                        borderColor: color, 
                        borderWidth: 1
                    }]
                }
            })
        });
    </script>

 
</body>
</html>